﻿#include "Layer.h"

bool Layer::init()
{
	if (!NodeGGE::init())
	{
		return false;
	}
	auto size = Director::getInstance()->getWinSize();
	_sprite = gge::Sprite_Create(nullptr, 0, 0, size.width, size.height);
	asert(_sprite, "ggesprite create fail");
	setColor(Color3B::BLACK);
	setOpacity(0);
	return true;
}


void Layer::setContentSize(const Size& contentSize)
{
	NodeGGE::setContentSize(contentSize);
	_sprite->SetTextureRect(0, 0, contentSize.width, contentSize.height);
}

void Layer::renderColor(gge::guint32 color)
{
	if (_sprite)
	{
		_sprite->SetColor(color);
	}
}

void Layer::render(uint32_t flags, const std::array<Vec2, 4>& vecRenders, const Vec2& vecAp)
{
	if (_sprite)
	{
		if (flags)
		{
			_sprite->SetPosition4V(
				vecRenders[0].x, vecRenders[0].y,
				vecRenders[1].x, vecRenders[1].y,
				vecRenders[2].x, vecRenders[2].y,
				vecRenders[3].x, vecRenders[3].y);
		}
		_sprite->RenderPosition();
	}
}


