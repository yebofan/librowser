#pragma once
#include "../libcocos2d/cocos2d.h"

#if defined(_WIN32)
#define ccc_mouse
#else
#define ccc_not_win32
#define asert
#define ccc_log(...) __android_log_print(ANDROID_LOG_DEBUG, "my_ccc_android: ", __VA_ARGS__)
#define ccc_msgbox
#endif




#define CREATE_INIT(_T_, _I_, ...)\
_T_ *pRet = new _T_();\
if (pRet && pRet->_I_(__VA_ARGS__))\
{\
	pRet->autorelease();\
	return pRet;\
}\
else\
{\
	CC_SAFE_DELETE(pRet);\
	return nullptr;\
}

#define CREATE(_TYPE_, ...) CREATE_INIT(_TYPE_, init, __VA_ARGS__)

USING_NS_CC;
namespace cc
{
	inline Vec2 vsOrigin(){ return Director::getInstance()->getVisibleOrigin(); }

	inline Size vsSize(){ return Director::getInstance()->getVisibleSize(); }

	inline Size vsSize(float x, float y)
	{
		Size size = Director::getInstance()->getVisibleSize();
		return Size(size.width * x, size.height * y);
	}

	inline Vec2 vsAt(float x, float y){ return vsOrigin() + vsSize(x, y); }

	inline Vec2 vsCenter(){ return vsOrigin() + vsSize() / 2; }

	inline Vec2 vsRand(){ return vsAt(random(0.0f, 1.0f), random(0.0f, 1.0f)); }

	inline float vsWide(){ return vsSize().width; }

	inline float vsHigh(){ return vsSize().height; }

	inline float vsX(float x_0_1){ return vsOrigin().x + vsWide() * x_0_1; }

	inline float vsY(float y_0_1){ return vsOrigin().y + vsHigh() * y_0_1; }



	////////////////////////////////////////////////////////////////////////// getNowSec
	inline double getNowSec()
	{
		timeval nowtime;
		gettimeofday(&nowtime, nullptr);
		return (long long)(nowtime.tv_sec) + nowtime.tv_usec / 1000000.0f;
	}
}