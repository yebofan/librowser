#pragma once
#include "_ccginc.h"

class GLViewImpl : public GLView
{
public:
	static GLViewImpl* createWithRect(const std::string& viewName, Rect rect){ CREATE(GLViewImpl, viewName, rect); }

	virtual void setCursorVisible(bool visible);
	virtual void setViewName(const std::string& viewname);
	virtual void setFrameSize(float width, float height);
protected:
	bool init(const std::string& viewName, Rect rect);

	virtual void end(){}
	virtual bool isOpenGLReady(){ return true; }
	virtual void swapBuffers(){}
	virtual void setIMEKeyboardState(bool open){}
	virtual HWND getWin32Window();
	virtual void setScissorInPoints(float x, float y, float w, float h);
	virtual void pollEvents();
	virtual void renderScene(Scene* scene, Renderer* renderer);


	bool _captured = false;
	int _isDeltaBasckspace = 0;
};