#include "AppDelegate.h"

bool AppGGE::OnConfig()
{
	_director = Director::getInstance();
	if (onApplicationDidFinishLaunching)
	{
		onApplicationDidFinishLaunching(_director);
	}
	return true;
}

bool AppGGE::OnInitiate()
{
// 	gge::System_SetState(gge::GGE_STATE_INT::GGE_SCREENWIDTH, w);
// 	gge::System_SetState(gge::GGE_STATE_INT::GGE_SCREENHEIGHT, h);
// 	gge::System_SetState(gge::GGE_STATE_CHAR::GGE_TITLE, "������");
// 	gge::System_SetState(gge::GGE_STATE_BOOL_FUN::GGE_FRAMEFUNC, [](){update(); });
// 	gge::System_SetState(gge::GGE_STATE_BOOL_FUN::GGE_FOCUSLOSTFUNC, [](){return applicationDidEnterBackground(); });
// 	gge::System_SetState(gge::GGE_STATE_BOOL_FUN::GGE_FOCUSGAINFUNC, [](){return applicationWillEnterForeground(); });
// 	gge::System_SetState(gge::GGE_STATE_MSG_FUN::GGE_MESSAGEFUNC, message);
// 	gge::System_SetState(gge::GGE_STATE_BOOL_FUN::GGE_EXITFUNC, exit);
// 	gge::System_SetState(gge::GGE_STATE_CHAR::GGE_LOGNAME, "xyq2d.log");
// 	gge::System_SetState(gge::GGE_STATE_BOOL::GGE_USESOUND, false);
	gge::System_SetState(gge::GGE_STATE_BOOL::GGE_VSYNC, true);
	gge::System_SetState(gge::GGE_STATE_BOOL::GGE_HIDEMOUSE, false);
	gge::System_SetState(gge::GGE_STATE_BOOL::GGE_DEBUGLOG, false);
//	gge::System_SetState(gge::GGE_STATE_INT::GGE_FPS, 60);
	gge::System_SetState(gge::GGE_STATE_INT::GGE_ICON, 101);

	if (onRunWithScene)
	{
		new AppDelegate();
		onRunWithScene(_director);
	}
	_director->getOpenGLView()->retain();
	return true;
}

void AppGGE::OnUpdate(float dt)
{
	if (gge::Input_IsKeyPress(GGEK_ALT) && gge::Input_IsKeyDown(GGEK_ENTER))
	{
//		SwitchSimFullScreen(!IsSimFullScreen());
	}
}

void AppGGE::OnRender()
{
	_director->mainLoop();
//	_director->getOpenGLView()->pollEvents();
}

void AppGGE::OnMiniDump(const char *dir)
{

}

void AppGGE::OnRelease()
{
	_director->end();
	_director->mainLoop();
//	_director->getOpenGLView()->release();
}