﻿#pragma once
#include "NodeGGE.h"

class LabelFont : public NodeGGE
{
public:
	static LabelFont* create(const std::string& fontname, int fontSize){ CREATE(LabelFont, fontname, fontSize); }
	virtual ~LabelFont(){ if (_fnt)_fnt->Release(); }
	virtual void setContentSize(const Size&) override{ asert(false, "label is not allow setContentSize"); }

	virtual void setString(const std::string& text);
	virtual std::string getString(){ return _text; }

	virtual void setLineWide(int w){ _fnt->SetLineWidth(w); }
	virtual int getLineWide(){ return _fnt->GetLineWidth(); }
	virtual void setCharNum(int number){ _fnt->SetCharNum(number); }

protected:
	virtual void renderColor(gge::guint32 color){ if(_fnt) _fnt->SetColor(color); }
	virtual void render(uint32_t flags, const std::array<Vec2, 4>& vecRenders, const Vec2& vecAp);

	virtual bool init(const std::string& fontname, int fontSize);
	gge::ggeFont *_fnt = nullptr;
	Vec2 _vecRender;
	std::string _text;
};