﻿#pragma once
#include "NodeGGE.h"

class Layer : public NodeGGE
{
public:
	virtual ~Layer(){ if (_sprite)_sprite->Release(); }
	CREATE_FUNC(Layer);
	virtual void setContentSize(const Size& contentSize);
protected:
	virtual void renderColor(gge::guint32 color);
	virtual void render(uint32_t flags, const std::array<Vec2, 4>& vecRenders, const Vec2& vecAp);

	virtual bool init();
	gge::ggeSprite* _sprite = nullptr;
};