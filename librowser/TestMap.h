#pragma once
#include "_inc.h"

class Map2 : public NodeMap, public NodeSnatch
{
public:
	static Map2* create(const string& filensame){ CREATE(Map2, filensame); }

	virtual bool init(const string & filename)
	{
		if (!NodeMap::init(filename))
		{
			return false;
		}
		if (!NodeSnatch::init(this))
		{
			return false;
		}
		setDragType(NodeSnatch::eDrag::Both);
		ListenerSnatch::create(this)->addChildSnatch(this, false);
		return true;
	}

	virtual void doClick(const Vec2& location, bool isActiveForListener)
	{
		if (isActiveForListener)
		{
			Vec2 v = this->convertToNodeSpace(location);
			loadAround(v);
		}
	}

	virtual void doDraging(const Vec2& worldSpace)
	{
		// use parent -> convertToNodeSpace for position
		Vec2 v = convertToNodeSpace(cc::vsOrigin());
		Vec2 v1;
		if (v.x < 0 || v.y < 0)
		{
			v1 = getParent()->convertToNodeSpace(convertToWorldSpace(Vec2::ZERO) - cc::vsOrigin());
			if (v.x < 0)
			{
				setPositionX(getPositionX() - v1.x);
			}
			if (v.y < 0)
			{
				setPositionY(getPositionY() - v1.y);
			}
		}
		v = convertToNodeSpace(cc::vsSize());
		if (v.x > _contentSize.width || v.y > _contentSize.height)
		{
			v1 = getParent()->convertToNodeSpace(convertToWorldSpace(_contentSize) - cc::vsSize());		
			if (v.x > _contentSize.width)
			{
				setPositionX(getPositionX() - v1.x);
			}
			if (v.y > _contentSize.height)
			{
				setPositionY(getPositionY() - v1.y);
			}
		}

	}
};


class TestMap : public Scene
{
public:
	CREATE_FUNC(TestMap);

	virtual bool init()
	{
		Scene::init();
		auto map = Map2::create("C:/Users/deen/Desktop/lxy/scene/1001.map");
//		map->setScale(0.5f);
		map->setScrollSpeed(50);
		addChild(map);
// 		ScrollView* scrollview = ScrollView::create();
// 		scrollview->setInnerSize(map->getContentSize() * map->getScale());
// 		scrollview->addChildSnatch(map);
// 		scrollview->setBounceable(false);
//		ListenerSnatch::create(this)->addChildSnatch(map);

// 		const auto& maps = cct::getMaps();
// 		const auto& npcs = cct::getNpcs();
// 		for (const auto& m : maps)
// 		{
// 			if (m.id != 1001)
// 			{
// 				continue;
// 			}
// 			for (const auto& npc : m.npcs)
// 			{
// 				forr(npcs, i)
// 				{
// 					if (npcs[i].e == npc.e)
// 					{
// 						const auto& p = cfg::getWdf5()->getPointer(npcs[i].stand);
// 						auto model = NodeModel::create(this, p, npc.name, "����");
// 						map->addChildSnatch(model);
// 						model->setPosition(npc.x * map->MAP_20, npc.y * map->MAP_20);
// 						model->setDirection((eDirection)(npc.direction % 4));
// 						model->setBlink(true);
// 						break;
// 					}
// 				}
// 			}
// 		}
		return true;
	}
};