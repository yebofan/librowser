#include "_inc.h"


class BtnPure : public NodeSnatch, public DrawNode
{
public:
	Label2* _label = nullptr;
	function<void(BtnPure*)> onClick = nullptr;
	static BtnPure* create(const string& text = ""){ CREATE(BtnPure, text); }
	virtual bool init(const string& text)
	{
		if (!DrawNode::init())
		{
			return false;
		}
		if (!NodeSnatch::init(this))
		{
			return false;
		}

		this->setContentSize(Size(cc::vsWide() / 2, 32));
		_label = Label2::create(text);
		_label->setPosition(_contentSize / 2);
		_label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		this->addChild(_label);
		DrawNode::drawRect(Vec2::ZERO, _contentSize, Color4F::WHITE);
		return true;
	}


	virtual void setState(const eState& state)
	{
		if (!_label)
		{
			return;
		}
		switch (state)
		{
		case eState::Normal:
			_label->setColor(Color3B::WHITE);
			break;
		case eState::Cross:
			_label->setColor(Color3B::YELLOW);
			break;
		case eState::Down:
			_label->setColor(Color3B::RED);
			break;
		case eState::None:
			_label->setColor(Color3B::GRAY);
			break;
		}
	}

	virtual void doClick(const Vec2& location, bool isActiveForListener)
	{
		if (isActiveForListener && onClick)
		{
			onClick(this);
		}
	}
};

class FiltrateBox : public Node
{
public:
	int minw, minh, minf, mind;
	int maxw, maxh, maxf, maxd;
	bool orderByName = true;
	vector<array<EditBox*, 2>> _eboxs;
	CREATE_FUNC(FiltrateBox);

	virtual bool init()
	{
		Node::init();

		int ih = 16;
		ListenerSnatch *pm = ListenerSnatch::create(this);

		setContentSize(Size(ih * 10, ih * 6));

		vector<string> txts = { " 宽 ", " 高 ", "方向", " 帧 " };
		_eboxs.resize(txts.size());
		forr(txts, i)
		{
			Label2 *label = Label2::create("=<" + txts[i] + "=<", ih);
			this->addChild(label);
			label->setPosition(ih * 5, (ih + 5) * (txts.size() - 1 - i));
			label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);

			EditBox* box = _eboxs[i][0] = EditBox::create("", 4, ih);
			box->setPosition(0, label->getPositionY());
			pm->addChildSnatch(box);

			EditBox* box2 = _eboxs[i][1] = EditBox::create("", 4, ih);
			box2->setPosition(ih * 8, label->getPositionY());
			pm->addChildSnatch(box2);
		}

		BtnPure* btn = BtnPure::create("名字排序");
		btn->setContentSize(Size(96, btn->getContentSize().height));
		btn->clear();
		btn->drawRect(Vec2::ZERO, btn->getContentSize(), Color4F::BLUE);
		btn->_label->setPosition(btn->getContentSize() / 2);
		btn->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
		pm->addChildSnatch(btn);
		btn->setPosition(ih * 5, (ih + 5) * txts.size() + 5);
		btn->onClick = [&](BtnPure* b)
		{
			if (b->_label->getString() == "名字排序")
			{
				b->_label->setString("大小排序");
				orderByName = false;
			}
			else
			{
				b->_label->setString("名字排序");
				orderByName = true;
			}
		};
		return true;
	}

	void updateData()
	{
		string str = _eboxs[0][0]->getString();
		minw = str.empty() ? 1 : atoi(str.c_str());

		str = _eboxs[1][0]->getString();
		minh = str.empty() ? 1 : atoi(str.c_str());

		str = _eboxs[3][0]->getString();
		minf = str.empty() ? 1 : atoi(str.c_str());

		str = _eboxs[2][0]->getString();
		mind = str.empty() ? 1 : atoi(str.c_str());

		str = _eboxs[0][1]->getString();
		maxw = str.empty() ? 2048 : atoi(str.c_str());

		str = _eboxs[1][1]->getString();
		maxh = str.empty() ? 2048 : atoi(str.c_str());

		str = _eboxs[3][1]->getString();
		maxf = str.empty() ? 64 : atoi(str.c_str());

		str = _eboxs[2][1]->getString();
		maxd = str.empty() ? 8 : atoi(str.c_str());
	}
};


inline  map<ulong, string> wasPack(string path)
{
	auto names = cc::efolder(path, true, 0);
	map<ulong, string> uMap;
	vector<ulong> uids;
	ulong uid;
	string str;
	for (const auto& name : names)
	{
		uid = std::strtoul(name.c_str(), nullptr, 16);
		uids.push_back(uid);
		if (uid > 0)
		{
			uMap.insert(make_pair(uid, name));
		}
		else
		{
			uMap.clear();
			return uMap;
		}
	}
	if (uids.size() != uMap.size())
	{
		uMap.clear();
	}
	return uMap;
}


class BtnGrop : public Node
{
public:
	map<string, map<ulong, string>> paths;
	function<void(const string& wdfname, const map<ulong, string>& files)> onCall = nullptr;
	CREATE_FUNC(BtnGrop);
	virtual bool init()
	{
		Node::init();

		auto names = cc::efolder("", false, 1);
		set<string> pathSet;
		map<ulong, string> uMap;
		paths.clear();
		string str;
		for (const auto& name : names)
		{
			const auto istr = name.find_last_of('/');
			if (istr == string::npos)
			{
				continue;
			}
			str = name.substr(0, istr + 1);

			if (pathSet.find(str) != pathSet.end())
			{
				continue;
			}
			pathSet.insert(str);

			printf("\n 分析 %s", str.c_str());

			uMap = wasPack(str);
			if (uMap.size() > 0)
			{
				paths.insert(make_pair(str, uMap));
			}
			else
			{
				printf(": 有0/重复/无效/空/的编号,请检查后再打包");
			}
		}

		if (paths.size() > 0)
		{
		//	system("md _打包完毕_\\");
		}


		ListenerSnatch *pm = ListenerSnatch::create(this);

		int size = paths.size();
		int h = 0;
		Size wh = BtnPure::create()->getContentSize();
		wh.width += 5;
		wh.height += h;
		int x = cc::vsWide() / 2 / (wh.width);
		int w = 0;
		for (auto& path : paths)
		{
			BtnPure* btn = BtnPure::create(path.first);
			if (w + wh.width > cc::vsWide())
			{
				h += wh.height;
				w = 0;
			}
			btn->setPosition(w, h);
			btn->setAnchorPoint(Vec2::ZERO);
			w += wh.width;
			pm->addChildSnatch(btn);
			btn->onClick = [&](BtnPure* b)
			{
				if (onCall)
				{
					onCall(b->_label->getString(), paths.at(b->_label->getString()));
				}
			};

// 			str = path.first;
// 			const auto& istr = str.find(s_unpack);
// 			if (istr != string::npos)
// 			{
// 				str.erase(istr + 1, s_unpack.length());
// 			}
// 			else if (str.back() == '/')
// 			{
// 				str.pop_back();
// 			}
// 
// 			wasPack(path.second, path.first, "_打包完毕_/" + str, true, false);
		}



		WdfReader reader;;
		reader.loads("");
		const auto& wdfs = reader.getWdfs();
		for (const auto& wdf : wdfs)
		{
			if (wdf.indexs == nullptr)
			{
				continue;
			}
			if (wdf.indexsCount == 0)
			{
				continue;
			}
			BtnPure* btn = BtnPure::create(wdf.filename);
			if (w + wh.width > cc::vsWide())
			{
				h += wh.height;
				w = 0;
			}
			btn->setPosition(w, h);
			btn->setAnchorPoint(Vec2::ZERO);
			w += wh.width;
			pm->addChildSnatch(btn);
			btn->onClick = [&](BtnPure* b)
			{
				if (onCall)
				{
					onCall(b->_label->getString(), map<ulong, string>());
				}
			};
		}

		this->setContentSize(Size(cc::vsWide() / 2, h + wh.height));

		return true;
	}


};

static bool isOk(FiltrateBox* box, const sSp& sp)
{
	if (sp.m_w < box->minw)
	{
		return false;
	}
	if (sp.m_h < box->minh)
	{
		return false;
	}
	if (sp.dCount < box->mind)
	{
		return false;
	}
	if (sp.fCountPD < box->minf)
	{
		return false;
	}

	if (sp.m_w > box->maxw)
	{
		return false;
	}
	if (sp.m_h > box->maxh)
	{
		return false;
	}
	if (sp.dCount > box->maxd)
	{
		return false;
	}
	if (sp.fCountPD > box->maxf)
	{
		return false;
	}
	return true;
}


class Filtrate : public PadWas
{
public:
	CREATE_FUNC(Filtrate);
	virtual bool init()
	{
		if (!PadWas::init())
		{
			return false;
		}
		setContentSize(cc::vsSize());

		BtnGrop* btns = BtnGrop::create();
		btns->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
		btns->setPosition(cc::vsAt(0.5f, 0.1f));
		this->addChild(btns);

		FiltrateBox* box = FiltrateBox::create();
		box->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
		box->setPosition(cc::vsAt(0.5f, 0.9f));
		this->addChild(box);

		btns->onCall = [this, box, btns](const string& wdfname, const map<ulong, string>& files)
		{
			static sSp sp;
			static char ptr18[18];
			box->updateData();
			if (files.empty())
			{
				WdfReader r;
				r.load(wdfname);
				const auto& wdf5 = r.getWdfs().front();

				struct sSpIdx
				{
					sSp sp;
					WdfReader::sIndex idx;
				};

				vector<sSpIdx> idxs;
				for (int i = 0; i < wdf5.indexsCount; ++i)
				{
					const auto& index5 = wdf5.indexs[i];
					FILE* file = fopen(wdfname.c_str(), "rb");
					fseek(file, index5.offset, SEEK_SET);
					fread(ptr18, 1, 18, file);
					if (!isSp(ptr18))
					{
						fclose(file);
						continue;
					}
					static sSp sp;
					getSp(sp, ptr18);
					if (!isOk(box, sp))
					{
						fclose(file);
						continue;
					}
					fclose(file);
					if (box->orderByName)
					{
						SpriteBtn* spr = SpriteBtn::create(r.getPointer(&index5, wdf5.filename));
						if (!spr)
						{
							continue;
						}
						initRank(spr, ccc_u2s(index5.uid), nullptr);
						continue;
					}
					idxs.push_back({ sp, index5 });
				}
				if (idxs.size())
				{
					std::sort(idxs.begin(), idxs.end(), [](const sSpIdx& a, const sSpIdx& b)
					{
						if (a.sp.m_w != b.sp.m_w)
						{
							return a.sp.m_w > b.sp.m_w;
						}
						return a.sp.m_h > b.sp.m_h;
					});

					forr(idxs, i)
					{
						const auto& idx = idxs[i].idx;
						SpriteBtn* spr = SpriteBtn::create(r.getPointer(&idx, wdf5.filename));
						if (!spr)
						{
							continue;
						}
						initRank(spr, ccc_u2s(idx.uid), nullptr);
					}
				}
			}
			else
			{
				struct sStrSp
				{
					ulong uid;
					string name;
					sSp sp;
				};
				vector<sStrSp> f2s;
				for (const auto& f : files)
				{
					FILE* file = fopen((wdfname + f.second).c_str(), "rb");
					fread(ptr18, 1, 18, file);
					if (!isSp(ptr18))
					{
						fclose(file);
						continue;
					}
					getSp(sp, ptr18);
					fclose(file);
					if (!isOk(box, sp))
					{
						continue;
					}
					f2s.push_back({f.first, f.second, sp});
				}
				if (!box->orderByName)
				{
					std::sort(f2s.begin(), f2s.end(), [](const sStrSp& a, const sStrSp& b)
					{
						if (a.sp.m_w != b.sp.m_w)
						{
							return a.sp.m_w < b.sp.m_w;
						}
						return a.sp.m_h < b.sp.m_h;
					});
				}

				string name;
				forv(f2s, i)
				{
					name = f2s[i].name;
					SpriteBtn* spr5 = SpriteBtn::create(readPointer(wdfname + name));
					if (!spr5)
					{
						continue;
					}
					initRank(spr5, name.substr(name.length() - 12, 8));
				}
			}
			initScroll();
			box->setVisible(false);
			btns->setVisible(false);
		};
		return true;
	}
};

class TestFiltrate : public Scene
{
public:
	CREATE_FUNC(TestFiltrate);
	virtual bool init()
	{
		Scene::init();
		ListenerSnatch *pm = ListenerSnatch::create(this);
		pm->addChildSnatch(Filtrate::create());
		return true;
	}
};