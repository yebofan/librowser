#pragma once
#include "../libcc/_string.h"
#include "../libcc/save.h"

#include "../libutil/_utils.h"

#include "../libcc/_window.h"


#define ccc_u2s(_uid_)toString("%08X", (_uid_))
#define ccc_u2was(_uid_)toString("%08X.was", (_uid_))
#define ccc_md(_path_)system(("md " + cc::replace((_path_), "/", "\\")).c_str())
#define ccc_move(_path_, _path_move_)system(("move " + cc::replace(((_path_) + " " + (_path_move_)), "/", "\\")).c_str())
#define ccc_copy(_name_, _name_copy_)system(("copy " + cc::replace(((_name_) + " " + (_name_copy_)), "/", "\\")).c_str())



// 需要主动释放内存
inline cpp::sPointer readPointer(string filename, const char* readmole = "rb")
{
	cpp::sPointer pter;
	FILE* file = fopen(filename.c_str(), readmole);
	fseek(file, 0, SEEK_END);
	pter.ptr = new char[pter.size = ftell(file)];
	fseek(file, 0, SEEK_SET);
	fread(pter.ptr, 1, pter.size, file);
	fclose(file);
	return pter;
}