#pragma once
#include "PadWas.h"
#include "../libcc/efolder.h"


class SpriteMove : public PadWas
{
public:
	string _path, _pathMoveTo;

	static SpriteMove* create(const string& path, const string& pathMoveTo = ""){ CREATE(SpriteMove, path, pathMoveTo); }

	virtual bool init(const string& path, const string& pathMoveTo)
	{
		if (!PadWas::init())
		{
			return false;
		}

		_path = path;
		_pathMoveTo = pathMoveTo;
		if (!_pathMoveTo.empty())
		{
			ccc_md(_pathMoveTo);
		}
		auto names = cc::efolder(path, false, 0);

		static sSp sp;
		static char ptr18[18];
		struct sStrSp{ string name; sSp sp; };
		vector<sStrSp> nameSps;
		for (const auto& name : names)
		{
			FILE* file = fopen(name.c_str(), "rb");
			fread(ptr18, 1, 18, file);
			if (!isSp(ptr18))
			{
				fclose(file);
				continue;
			}
			getSp(sp, ptr18);
			fclose(file);
// 			if (abs(sp.m_h - 40) < 4 && abs(sp.m_w - 40) < 4 && sp.fCountPD == 1)
// 			if (sp.fCountPD == 1 && sp.dCount == 1)
//			if (sp.m_w >= 540 && sp.m_w < 800 && sp.m_h >= 360 && sp.m_h < 600)
// 			if (abs(sp.m_w - 32) < 3 && abs(sp.m_h - 32) < 3 /*&& abs(sp.m_w - sp.m_h) > 0*/)
//			if (sp.m_w == 40 && sp.m_h == 40 && sp.dCount == 1 && sp.fCountPD == 1)
//			if (abs(sp.m_w - 40) < 10 && abs(sp.m_h - 40) < 10 && sp.fCountPD == 1 && sp.dCount == 1)
			{
				nameSps.push_back({ name, sp });
			}
		}
		std::sort(nameSps.begin(), nameSps.end(), [](const sStrSp &s1, const sStrSp& s2)
		{
			if (s1.sp.m_w != s2.sp.m_w)
			{
				return s1.sp.m_w < s2.sp.m_w;
			}
			return s1.sp.m_h < s2.sp.m_h;
		});

		for (const auto& ss : nameSps)
		{
			const auto& name = ss.name;

			SpriteBtn* spr5 = SpriteBtn::create(readPointer(name));
			initRank(spr5, name.substr(name.length() - 12, 8));
		}

		initScroll();

		onKeyEnter = [&](const vector<string>& moves)
		{
			if (!_pathMoveTo.empty() && !moves.empty())
			{
				for (const auto& m : moves)
				{
					ccc_move(_path + m + ".was", _pathMoveTo);
				}
				this->removeFromParent();
			}
		};
		return true;
	}

};

