#pragma once
#include "_inc.h"
#include <fstream>


class SpriteBtn : public Sprite2, public NodeSnatch
{
public:
	static SpriteBtn* create(const cpp::sPointer& pointer){ CREATE(SpriteBtn, pointer); }
	virtual bool init(const cpp::sPointer& pointer){ return NodeSnatch::init(this) && Sprite2::init(pointer/*, nullptr*/); }
	function<void()> onClick = nullptr;
	virtual void doClick(const Vec2& location, bool isActiveForListener)
	{
		if (isActiveForListener && onClick)
		{
			onClick();
		}
	}
	int getKx(){ return _decoder.info->kx; }
	int getKy() {return _decoder.info->ky;}
	bool isAllTexturesLoaded()
	{
		for (int k = _decoder.info->framesCount - 1; k >= 0; --k)
		{
			if (_textures[k] == nullptr)
			{
				return false;
			}
		}
		return true;
	}
	bool savePNG(const string& filename)
	{
		string str;
	//	int x, y, w;
		static uint* data = new uint[4096 * 4096];

		const auto& info = _decoder.info;
		for (int k = info->framesCount - 1; k >= 0; --k)
		{
		//	memset(data, 0, _was.wMax * _was.hMax * sizeof(uint)); 
			const auto& fr = info->frames[k];
			auto deco = _decoder.getDecode(k);
			auto p565 = _decoder.get565(k);
			for (int i = fr.w * fr.h - 1; i >= 0; --i)
			{
				data[i] = p565[i].to8888(deco->alphas[i]);
			}
// 			auto ptr = (ushort*)_textures[k]->Lock(true);
// 			x =  _was.kx - fr.x;
// 			y =  _was.ky - fr.y;
// 			w = _textures[k]->GetWidth();
// 			for (int i = _textures[k]->GetHeight() - 1; i >= 0; --i)
// 			{
// 				uint* c4 = &data[((y + i) * _was.wMax) + x];
// 				memcpy(c4, ptr + (i * w), w * sizeof(uint));
// 
// 				for (int j = w - 1; j >= 0; --j)
// 				{	
// 					if (c4)
// 					{
// 						c = (uchar*)&c4[j];
// 						std::swap(c[0], c[2]);
// 					//	c[3] = 0x88;
// 					}
// 				}
// 			}
// 
// 			_textures[k]->Unlock();
			str = filename;
			str += toString("-%02d-%02d.png", k / info->framesCountPerDirection, k % info->framesCountPerDirection);
			save2png(str.c_str(), (uchar*)data, fr.w, fr.h);
		}
//		delete[] data;
		return true;
	}
};



class PadWas : public ScrollView
{
	int inv = 20;
	int x = inv;
	int y = inv;
	float h = 0;
	Size size, size2;
	int _i = 0;
	vector<Label2*> _labels;
protected:
	function<void(const vector<string>&)> onKeyEnter = nullptr;

	virtual bool init()
	{
		if (!ScrollView::init(cc::vsSize(), cc::vsSize()))
		{
			return false;
		}
//		setPosition(200, 200);
		setScrollSpeed(100);
		setDragType(eDrag::V);
		return true;
	}

	void initRank(SpriteBtn* spr, string sid, Sprite2* spr2 = nullptr)
	{
		if (!spr)
		{
			return;
		}

		Node* node = Node::create();
		node->addChild(spr);
		Vec2 v = spr->getAnchorPointInPoints();
		Vec2 v2(2, 2);
		spr->setPosition(v);
		spr->play(true);
		Label2* label = Label2::create(sid, 14);
		label->setColor(Color3B::GREEN);
		label->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
		node->addChild(label);
		label->setVisible(false);
		_labels.push_back(label);
		label->setUserData(spr);

		if (spr->getDirectionsCount() > 1)
		{
			Label2* label2 = Label2::create(cc::toString(spr->getDirectionsCount()));
			label2->setPositionX(2);
			label2->setColor(Color3B::GREEN);
			node->addChild(label2);
		}
		if (spr->getFrameCountPD() > 1)
		{
			DrawNode* dn = DrawNode::create();
			node->addChild(dn);
			dn->drawRect(v - v2, v + v2, Color4F::RED);

			Label2* label2 = Label2::create(cc::toString(spr->getFrameCountPD()));
			label2->setPositionX(spr->getContentSize().width - label2->getContentSize().width - 2);
//			label2->setColor(Color3B::MAGENTA);
			node->addChild(label2);
		}

		Node* node2 = nullptr;
		if (spr2)
		{
			node2 = Node::create();
			node2->addChild(spr2);
			v = spr2->getAnchorPointInPoints();
			spr2->setPosition(v);
			spr2->play(true);

			if (spr2->getFrameCountPD() > 1)
			{
				DrawNode* dn = DrawNode::create();
				node2->addChild(dn);
				dn->drawRect(v - v2, v + v2, Color4F::RED);
			}
		}

		size = spr->getContentSize();
		if (spr2)
		{
			size2 = spr2->getContentSize();
		}
		else
		{
			size2 = Size::ZERO;
		}

		if (_i > 0)
		{
			if (x + size2.width + size.width + inv * 2 > cc::vsWide())
			{
				x = inv;
				y += h + inv;
				h = 0;
			}
		}
		node->setPosition(x, y);
		if (node2)
		{
			node2->setPosition(x + size.width + inv, y);
		}

		x += size2.width + size.width + inv * 2;
		h = std::max(h, std::max(size2.height, size.height));

		DrawNode* dn = DrawNode::create();
		node->addChild(dn);
		dn->drawRect(Vec2::ZERO, size, Color4F::YELLOW);

		if (node2)
		{
			dn = DrawNode::create();
			node2->addChild(dn);
			dn->drawRect(Vec2::ZERO, size2, Color4F::YELLOW);
		}


		ScrollView::addChild(node);
		ScrollView::addChildSnatch(spr, false);
		spr->onClick = [label]()
		{
			bool visible = label->isVisible();
			label->setVisible(!visible);
			if (!visible)
			{
				string s = "0x";
				s += label->getString() + ",";
				OutputDebugStringA(s.c_str());
//				ccc_log(s.c_str());
			}
		};
		if (node2)
		{
			ScrollView::addChild(node2);
		}
		++_i;
	}

	void initScroll()
	{
		ScrollView::setInnerSize(Size(cc::vsWide(), h + y));
		DrawNode* dn = DrawNode::create();
		ScrollView::addChild(dn);
		dn->drawRect(Vec2::ZERO, ScrollView::getInnerSize(), Color4F::WHITE);

// 		EventListenerMouse* mouse = EventListenerMouse::create();
// 		mouse->onMouseScroll = [&](EventMouse* e)
// 		{
// 			this->setPositionY(this->getPositionY() + e->getScrollY() * 500);
// 		};
// 		_eventDispatcher->addEventListenerWithSceneGraphPriority(mouse, this);


		EventListenerKeyboard *keyboard = EventListenerKeyboard::create();
		keyboard->onKeyPressed = [&](EventKeyboard::KeyCode code, Event* e)
		{
			if (code == EventKeyboard::KeyCode::KEY_TAB)
			{
				bool visible = _labels.front()->isVisible();
				for (const auto& l : _labels)
				{
					l->setVisible(!visible);
				}
				if (!visible)
				{
					int i = 0;
					for (const auto& l : _labels)
					{
						string s = "0x";
						s += l->getString() + ",";
						if ((++i) % 10 == 0)
						{
							s += '\n';
						}
						OutputDebugStringA(s.c_str());
					}
				}
			}
			else if (code == EventKeyboard::KeyCode::KEY_ENTER)
			{
			//	return;
				ccc_log("Enter: Begin");
				vector<string> moves;
				for (const auto& l : _labels)
				{
					if (l->isVisible())
					{
						ccc_log(l->getString().c_str());
						moves.push_back(l->getString());
					}
				}
				ccc_log("Enter: End %d", moves.size());
				if (onKeyEnter)
				{
					onKeyEnter(moves);
				}

				if (moves.empty())
				{
					return;
				}

				for (const auto& label : _labels)
				{
					if (!label->isVisible())
					{
						continue;
					}
					SpriteBtn* spr = (SpriteBtn*)label->getUserData();
					if (!spr->isAllTexturesLoaded())
					{
						ccc_msgbox("加载未完成", "请稍等");
						return;
					}
				}
				
				string path;
				std::ofstream ofile;
				for (const auto& label : _labels)
				{
					if (!label->isVisible())
					{
						continue;
					}
					path = "导出图片/" + label->getString() + "/";
					ccc_md(path);
					SpriteBtn* spr = (SpriteBtn*)label->getUserData();
					ofile.open(path + "_锚点.txt");
					ofile << spr->getKx() << "\n" << spr->getKy();
					ofile.close();
					spr->savePNG(path + label->getString());
				}
			}
			else if (code == EventKeyboard::KeyCode::KEY_SPACE)
			{
				OutputDebugStringA("0x00000000,");
			}
		};
		_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboard, this);
	}
};
