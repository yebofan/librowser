#pragma once

#include "base/base64.h"
#include "base/CCAutoreleasePool.h"
#include "base/ccCArray.h"
#include "base/ccConfig.h"
#include "base/CCDirector.h"
#include "base/CCIMEDelegate.h"
#include "base/CCIMEDispatcher.h"
#include "base/ccMacros.h"
#include "base/CCMap.h"
#include "base/ccRandom.h"
#include "base/CCRef.h"
#include "base/CCScheduler.h"
#include "base/ccTypes.h"
#include "base/CCVector.h"
#include "base/uthash.h"
#include "base/utlist.h"


// EventDispatcher
#include "base/CCController.h"
#include "base/CCEvent.h"
#include "base/CCEventAcceleration.h"
#include "base/CCEventController.h"
#include "base/CCEventCustom.h"
#include "base/CCEventDispatcher.h"
#include "base/CCEventFocus.h"
#include "base/CCEventKeyboard.h"
#include "base/CCEventListener.h"
#include "base/CCEventListenerAcceleration.h"
#include "base/CCEventListenerController.h"
#include "base/CCEventListenerCustom.h"
#include "base/CCEventListenerFocus.h"
#include "base/CCEventListenerKeyboard.h"
#include "base/CCEventListenerMouse.h"
#include "base/CCEventListenerTouch.h"
#include "base/CCEventMouse.h"
#include "base/CCEventTouch.h"
#include "base/CCEventType.h"
#include "base/CCTouch.h"


// math
#include "math/CCAffineTransform.h"
#include "math/CCGeometry.h"
#include "math/CCMath.h"
#include "math/CCMathBase.h"
#include "math/Mat4.h"
#include "math/MathUtil.h"
#include "math/Quaternion.h"
#include "math/TransformUtils.h"
#include "math/Vec2.h"
#include "math/Vec3.h"
#include "math/Vec4.h"

// actions
#include "2d/CCAction.h"
#include "2d/CCActionCatmullRom.h"
#include "2d/CCActionEase.h"
#include "2d/CCActionInstant.h"
#include "2d/CCActionInterval.h"
#include "2d/CCActionManager.h"
#include "2d/CCActionTween.h"
#include "2d/CCTweenFunction.h"

// 2d nodes
#include "2d/CCNode.h"
#include "2d/CCScene.h"
#include "2d/CCTransition.h"



// platform
#include "platform/CCApplication.h"
#include "platform/CCApplicationProtocol.h"
#include "platform/CCCommon.h"
#include "platform/CCGLView.h"
#include "platform/CCPlatformConfig.h"
#include "platform/CCPlatformDefine.h"
#include "platform/CCPlatformMacros.h"
#include "platform/CCStdC.h"

