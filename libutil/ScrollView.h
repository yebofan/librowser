﻿#pragma once
#include "NodeRect.h"
#include "NodeSnatch.h"

class ScrollView : public NodeSnatch, public NodeRect
{
public:
	enum class Direction{ VERTICAL, HORIZONTAL, BOTH };
	static ScrollView* create(const Size& size, const Size& innerSize){ CREATE(ScrollView, size, innerSize); }
	static ScrollView* create(const Size& innerSize){ CREATE(ScrollView, cc::vsSize(), innerSize); }
	static ScrollView* create(){ CREATE(ScrollView, cc::vsSize(), cc::vsSize()); }

	virtual void setContentSize(const Size& contentSize);

	virtual void setInnerSize(Size size){ _innerNode->setContentSize(size);}
	virtual Size getInnerSize(){ return _innerNode->getContentSize(); }

	virtual void setInnerPosition(int x, int y){ setInnerPosition(Vec2(x, y)); }
	virtual void setInnerPosition(Vec2 v){ _innerNode->setPosition(v); }
	virtual Vec2 getInnerPosition(){ return _innerNode->getPosition(); }

	// 回弹
	virtual void setBounceable(bool isBounceable){ _isBounceable = isBounceable; }

	virtual void addChild(Node * child, int z, int tag){ _innerNode->addChild(child, z, tag); }
	virtual void addChild(Node * child, int z){ _innerNode->addChild(child, z, Node::INVALID_TAG); }
	virtual void addChild(Node * child){ _innerNode->addChild(child, 0, Node::INVALID_TAG); }

	virtual Node* getInnerNode(){ return _innerNode; }
protected:
	virtual bool init(const Size& size, const Size& innerSize);
	virtual bool isBox(const Vec2& worldSpace){ return Rect(Vec2::ZERO, _contentSize).containsPoint(convertToNodeSpace(worldSpace)); }
	virtual void doDraging(const Vec2& worldSpace);
	virtual Node* getDragableNode(){ return getInnerNode(); }
	virtual Vec2 trimPosition(Vec2 v, float tolerance);

	Node *_innerNode = nullptr;
	bool _isBounceable = true;
};