﻿#pragma once
#include "_utilsinc.h"

class Label2 : public LabelFont
{
public:
	static const int FONT_SIZE_NORMAL = 16;
	static const int FONT_SIZE_SMALL= 20;
	static const int FONT_SIZE_BIG = 24;
	static Label2* create(const std::string& text = "", int fontSize = FONT_SIZE_NORMAL){ CREATE(Label2, text, fontSize); }
protected:
	virtual bool init(const std::string& text, int fontSize);
};