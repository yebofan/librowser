#pragma once
#include "_utilsinc.h"

class ButtonItem
{
public:
	enum class eState{ Normal, Cross, Down, None };
	virtual void setEnabled(bool isEnabled);
	virtual bool isEnabled()const{ return _isEnabled; }
	
protected:
	virtual ~ButtonItem();
	virtual bool init(Node* node);
	virtual Rect getBox();
	virtual void setState(const eState& state){}
	// ButtonItem: isActiveForListener
	// PadItem: isButtonClicking
	virtual void doClick(const Vec2& location, bool isActiveForListener){}
	virtual void doKeep(float delta){}

	bool _isEnabled = true;
	Node* _nodeTarget = nullptr;
	
	friend class PadItem;
	friend class PadListener;
	PadItem* _pad = nullptr;
};


class PadItem : public ButtonItem
{
public:
	enum class eDrag { None, H, V, Both };
	virtual ~PadItem();
	virtual void addChildSnatch(ButtonItem *btn, bool isAddchild = true);
protected:
	virtual void removeChildSnatch(ButtonItem *btn);
	
public:
	virtual void setDragType(const eDrag& e){ _dragType = e; }
	virtual void setCloseRup(bool isEnable){ _isCloseRup = isEnable; }
	virtual void setScrollSpeed(int speed){ _scrollSpeed = speed; }
	virtual Node* getDragableNode(){ return _nodeTarget; }

	virtual bool init(Node* node);
protected:
	virtual void doDraging(){}
	virtual ButtonItem* getButton(const Vec2& location);
	virtual void doVisible(bool visible);

	eDrag _dragType = eDrag::None;
	bool _isCloseRup = false;
	int _scrollSpeed = 0;

	vector<ButtonItem*> _buttons;

	friend class ButtonItem;
	friend class PadListener;
	PadListener* _palListener = nullptr;
};

class PadListener : public EventListenerTouchOneByOne
{
public:
	virtual ~PadListener();
	static PadListener* create(Node* node){ CREATE(PadListener, node); }
	virtual void addChildSnatch(PadItem* pad, bool isAddchild = true);
protected:
	virtual void removeChildSnatch(PadItem *pad);
	virtual bool init(Node* node);
private:
	void resortPad();
	PadItem* getPadTop(const Vec2& location);
	bool setPadForMouse(const Vec2& location);
	bool setButtonForMouse(const Vec2& location);
	bool setPadForTouch(const Vec2& location);
	bool setButtonForTouch(const Vec2& location);
	virtual void update(float delta);

	PadItem* _padTop = nullptr;
	ButtonItem* _btn = nullptr;
	int _z = 0;
	Vec2 _vecDrag, _vecStartLocation;
	bool _isDragThreshold = false;
	vector<PadItem*> _sortPads;
	float _delta;
	string _shedulerKey;
	bool _isSheduling = false;
	friend class PadItem;
};