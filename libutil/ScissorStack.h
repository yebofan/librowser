﻿#pragma once
#include "_utilsinc.h"

/************************************************************************
不要和手工的glview->setScissorInPoints混用。
************************************************************************/

class ScissorStack
{
public:
	//x, y, w, h都是屏幕上的GL像素点（硬件像素）
	//一个Node需要得到硬件像素，需要把自身的点converToWorldSpace
	static ScissorStack* getInstance();

	void setEnabled(bool isEnabled)
	{
		_isEnabled = isEnabled;
	}

	void push(const Rect& rect);
	void pop();
private:
	bool _isEnabled = false;
	std::stack<cocos2d::Rect> _stack;

};