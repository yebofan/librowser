#pragma once
#include "Sprite1.h"


class Sprite2 : public Node, public Sprite1
{
public:
	static Sprite2* create(const cpp::sPointer& pointer/*, const WasReader::sColor* colorData = nullptr*/)
	{
		CREATE(Sprite2, pointer/*, colorData*/);
	}

	virtual bool isBox(const Vec2& worldSpace);
protected:
	virtual bool init(const cpp::sPointer& pointer/*, const WasReader::sColor* colorData*/);
	virtual bool initSprite();
	virtual bool setTexture(Texture2D* texture, const sSpFrame& frame);
	virtual void update(float delta){ Sprite1::update(delta); }
	virtual void scheduleOn(){ scheduleUpdate(); }
	virtual void scheduleOff(){ unscheduleUpdate(); }
	virtual void doFrame(int frameIndex){ if (onFrame)onFrame(this, frameIndex); }

	Sprite* _sprite = nullptr;
	bool _isLight = false;
public:
	function<void(Sprite2*, int)> onFrame = nullptr;
};