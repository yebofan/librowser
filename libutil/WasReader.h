﻿#pragma once
#include "../libcc/s565.h"

// 内存 4*4
struct sSpFrame
{
	int x = 0, y = 0, w = 0, h = 0;

	bool isValid() const;
};

// 内存 2*6
struct sSpInfo
{
	struct sInfo
	{
		ushort directionsCount;
		ushort framesCountPerDirection;
		ushort width, height;
		short kx, ky;
	};
	int directionsCount = 0;
	int framesCountPerDirection = 0;
	int width = 0, height = 0;
	int kx = 0, ky = 0;
	int framesCount = 0;

	const sSpFrame *frames = nullptr;

	~sSpInfo();

	void load(const sInfo& info);

	void load(const char *ptr);

	bool isValid() const;
};


struct sDecode
{
	int width = 0, height = 0;
	uchar *indexs = nullptr;
	uchar *alphas = nullptr;
	s565 *normal565 = nullptr;
	bool isDecoded = false;

	sDecode();
	~sDecode();
	bool isValid() const;
	void decode(const uchar *pHead, uint offs);
	static void to565(const uchar *indexs, const uchar *alphas, int width, int height, const s565* palettes, s565* datas);
	const s565* get565(const s565* palettes);
};



struct sDecoderEasy
{
	const char *ptr = nullptr;
	const sSpInfo *info = nullptr;
	const s565 *palette = nullptr;
	const uint *offsets = nullptr;
	sDecode *decodes = nullptr;


	sDecoderEasy();
	~sDecoderEasy();

	void load(const char *ptr, const sSpInfo *info);

	sDecode* getDecode(int iframe);

	bool isSnatch(int x, int y, int iframe);

	const s565* get565(sDecode* deco) { return deco->get565(palette); }

	const s565* get565(int iframe);
};



// class WasReader
// {
// public:
// 	struct sInfo
// 	{
// 		ushort directionsCount;
// 		ushort framesCountPerDirection;
// 		ushort width, height;
// 		short kx, ky;
// 	};
// 
// 	const sSpFrame *frames = nullptr;
// 
// 	struct sFrameData{ unsigned short* data = nullptr; unsigned char* alpha = nullptr; };
// 
// 	int dCount, fCountPD, wMax, hMax, w, h, kx, ky;
// 
// 	void load(const sInfo& info);
// 
// 	void load(const char *ptr);
// 
// 	sFrameData getFrameData(int i);
// 
// 	void clearPointer();
// 
// 	~WasReader();
// };


