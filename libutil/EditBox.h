﻿#pragma once
#include "Label2.h"
#include "NodeSnatch.h"


class EditBox : public DrawNode, public NodeSnatch
{
public:
	static EditBox* create(const std::string &placeHolder, int limit, int fntSize = Label2::FONT_SIZE_NORMAL)
	{
		CREATE(EditBox, placeHolder, limit, fntSize);
	}

	virtual void setContentSize(const Size& contentSize);

	virtual void doClick(const Vec2& location, bool isActiveForListener);


	/**
	* Query the color of place holder.
	*@return The place holder color.
	*/
//	virtual const Color3B& getColorSpaceHolder(){ return _labelPlaceHolder->getColor(); }

	/**
	*@brief Change input placeholder color.
	*@param color A color value in `Color3B`.
	*/
//	virtual void setColorSpaceHolder(const Color3B& color){ _labelPlaceHolder->setColor(color); }

	/**
	* Change the color of input text.
	*@param textColor The text color in Color4B.
	*/
	virtual void setTextColor(const Color3B& textColor){ _label->setColor(textColor); }

	/**
	* Change input text of TextField.
	*@param text The input text of TextField.
	*/
	virtual void setString(const std::string& text){ _label->setString(text); }


	/**
	* Query the input text of TextField.
	*@return Get the input text of TextField.
	*/
	virtual std::string getString() { return _label->getString(); }

	/**
	* Change placeholder text.
	* place holder text displayed when there is no text in the text field.
	*@param text  The placeholder string.
	*/
//	virtual void setPlaceHolder(const std::string& text){ _labelPlaceHolder->setString(text); }

	/**
	* Query the placeholder string.
	*@return The placeholder string.
	*/
//	virtual const std::string& getPlaceHolder() const{ return _labelPlaceHolder->getString(); }

protected:
	virtual bool init(const std::string &placeHolder, int limit, int fntSize);

	virtual void insertText(const char* text, size_t len);

	virtual void deleteBackward();

	virtual void controlKey(EventKeyboard::KeyCode keyCode);

	int _limit;
	int _charCount = 0;
	Label2* _label = nullptr;
	Label2* _labelPlaceHolder = nullptr;
	Color3B _colorLine = Color3B::WHITE;
	LayerColor* _layerCursor = nullptr;
	friend class EditBoxDelegate;
};



class EditBoxDelegate : public IMEDelegate
{
public:
	static EditBoxDelegate* getInstance();

	virtual void setEditBox(EditBox* editBox);

	virtual bool canAttachWithIME() { return true; }

	virtual bool canDetachWithIME() { return true; }

	virtual void insertText(const char* text, size_t len);

	virtual void deleteBackward();

	virtual void controlKey(EventKeyboard::KeyCode keyCode);

private:
	EditBox* _currEditBox = nullptr;
};